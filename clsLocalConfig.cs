﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MailModule
{
    public class clsLocalConfig
    {
        private const string cstrID_Ontology = "a8a4c80a6b694f0b8e1ace2be541032c";
        private XMLImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        // Attributetypes
        public clsOntologyItem OItem_attributetype_usessl { get; set; }
        public clsOntologyItem OItem_attributetype_validateservercertificate { get; set; }

        // Classes
        public clsOntologyItem OItem_class_mail_connection { get; set; }
        public clsOntologyItem OItem_class_port { get; set; }
        public clsOntologyItem OItem_class_securesocketoptions { get; set; }
        public clsOntologyItem OItem_class_server { get; set; }
        public clsOntologyItem OItem_class_server_port { get; set; }
        public clsOntologyItem OItem_class_user { get; set; }
        public clsOntologyItem OItem_class_indexes__elastic_search_ { get; set; }
        public clsOntologyItem OItem_class_mailmodule { get; set; }
        public clsOntologyItem OItem_class_types__elastic_search_ { get; set; }

        // Objects
        public clsOntologyItem OItem_object_mailmodule { get; set; }

        // RelationType
        public clsOntologyItem OItem_relationtype_authorized_by { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
        public clsOntologyItem OItem_relationtype_uses { get; set; }
        public clsOntologyItem OItem_relationtype_smtp_host { get; set; }
        public clsOntologyItem OItem_relationtype_pop3_host { get; set; }
        public clsOntologyItem OItem_relationtype_imap_host { get; set; }


        private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                    }).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingClass.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingObject.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }

        public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }

        private void set_DBConnection()
        {
            objDBLevel_Config1 = new OntologyModDBConnector(Globals);
            objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            objImport = new XMLImportWorker(Globals);
        }

        private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            catch (Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }

                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                }
                else
                {
                    throw new Exception("Config not importable");
                }

            }
        }

        private void get_Config_AttributeTypes()
        {
            var objOList_attributetype_usessl = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attributetype_usessl".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

            if (objOList_attributetype_usessl.Any())
            {
                OItem_attributetype_usessl = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_usessl.First().ID_Other,
                    Name = objOList_attributetype_usessl.First().Name_Other,
                    GUID_Parent = objOList_attributetype_usessl.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_validateservercertificate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "attributetype_validateservercertificate".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                                    select objRef).ToList();

            if (objOList_attributetype_validateservercertificate.Any())
            {
                OItem_attributetype_validateservercertificate = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_validateservercertificate.First().ID_Other,
                    Name = objOList_attributetype_validateservercertificate.First().Name_Other,
                    GUID_Parent = objOList_attributetype_validateservercertificate.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_RelationTypes()
        {
            var objOList_relationtype_smtp_host = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_smtp_host".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_smtp_host.Any())
            {
                OItem_relationtype_smtp_host = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_smtp_host.First().ID_Other,
                    Name = objOList_relationtype_smtp_host.First().Name_Other,
                    GUID_Parent = objOList_relationtype_smtp_host.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_pop3_host = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_pop3_host".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_pop3_host.Any())
            {
                OItem_relationtype_pop3_host = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_pop3_host.First().ID_Other,
                    Name = objOList_relationtype_pop3_host.First().Name_Other,
                    GUID_Parent = objOList_relationtype_pop3_host.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_imap_host = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_imap_host".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_imap_host.Any())
            {
                OItem_relationtype_imap_host = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_imap_host.First().ID_Other,
                    Name = objOList_relationtype_imap_host.First().Name_Other,
                    GUID_Parent = objOList_relationtype_imap_host.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_authorized_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_authorized_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_authorized_by.Any())
            {
                OItem_relationtype_authorized_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_authorized_by.First().ID_Other,
                    Name = objOList_relationtype_authorized_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_authorized_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_belonging_source.Any())
            {
                OItem_relationtype_belonging_source = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_source.First().ID_Other,
                    Name = objOList_relationtype_belonging_source.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_uses = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_uses".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_uses.Any())
            {
                OItem_relationtype_uses = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_uses.First().ID_Other,
                    Name = objOList_relationtype_uses.First().Name_Other,
                    GUID_Parent = objOList_relationtype_uses.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Objects()
        {
            var objOList_object_mailmodule = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_mailmodule".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_mailmodule.Any())
            {
                OItem_object_mailmodule = new clsOntologyItem()
                {
                    GUID = objOList_object_mailmodule.First().ID_Other,
                    Name = objOList_object_mailmodule.First().Name_Other,
                    GUID_Parent = objOList_object_mailmodule.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Classes()
        {
            var objOList_class_types__elastic_search_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "class_types__elastic_search_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                         select objRef).ToList();

            if (objOList_class_types__elastic_search_.Any())
            {
                OItem_class_types__elastic_search_ = new clsOntologyItem()
                {
                    GUID = objOList_class_types__elastic_search_.First().ID_Other,
                    Name = objOList_class_types__elastic_search_.First().Name_Other,
                    GUID_Parent = objOList_class_types__elastic_search_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_class_mailmodule = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_mailmodule".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_class_mailmodule.Any())
            {
                OItem_class_mailmodule = new clsOntologyItem()
                {
                    GUID = objOList_class_mailmodule.First().ID_Other,
                    Name = objOList_class_mailmodule.First().Name_Other,
                    GUID_Parent = objOList_class_mailmodule.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_indexes__elastic_search_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                           where objOItem.ID_Object == cstrID_Ontology
                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                           where objRef.Name_Object.ToLower() == "class_indexes__elastic_search_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                           select objRef).ToList();

            if (objOList_class_indexes__elastic_search_.Any())
            {
                OItem_class_indexes__elastic_search_ = new clsOntologyItem()
                {
                    GUID = objOList_class_indexes__elastic_search_.First().ID_Other,
                    Name = objOList_class_indexes__elastic_search_.First().Name_Other,
                    GUID_Parent = objOList_class_indexes__elastic_search_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_mail_connection = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "class_mail_connection".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

            if (objOList_class_mail_connection.Any())
            {
                OItem_class_mail_connection = new clsOntologyItem()
                {
                    GUID = objOList_class_mail_connection.First().ID_Other,
                    Name = objOList_class_mail_connection.First().Name_Other,
                    GUID_Parent = objOList_class_mail_connection.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_port.Any())
            {
                OItem_class_port = new clsOntologyItem()
                {
                    GUID = objOList_class_port.First().ID_Other,
                    Name = objOList_class_port.First().Name_Other,
                    GUID_Parent = objOList_class_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_securesocketoptions = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "class_securesocketoptions".ToLower() && objRef.Ontology == Globals.Type_Class
                                                      select objRef).ToList();

            if (objOList_class_securesocketoptions.Any())
            {
                OItem_class_securesocketoptions = new clsOntologyItem()
                {
                    GUID = objOList_class_securesocketoptions.First().ID_Other,
                    Name = objOList_class_securesocketoptions.First().Name_Other,
                    GUID_Parent = objOList_class_securesocketoptions.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_class_server.Any())
            {
                OItem_class_server = new clsOntologyItem()
                {
                    GUID = objOList_class_server.First().ID_Other,
                    Name = objOList_class_server.First().Name_Other,
                    GUID_Parent = objOList_class_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_server_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "class_server_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_class_server_port.Any())
            {
                OItem_class_server_port = new clsOntologyItem()
                {
                    GUID = objOList_class_server_port.First().ID_Other,
                    Name = objOList_class_server_port.First().Name_Other,
                    GUID_Parent = objOList_class_server_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_user.Any())
            {
                OItem_class_user = new clsOntologyItem()
                {
                    GUID = objOList_class_user.First().ID_Other,
                    Name = objOList_class_user.First().Name_Other,
                    GUID_Parent = objOList_class_user.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }
    }

}
