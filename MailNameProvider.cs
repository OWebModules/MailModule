﻿using MailModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MailModule
{
    public class MailNameProvider : INameTransform
    {
        public bool IsReferenceCompatible => true;

        public bool IsAspNetProject { get; set; }

        private Globals globals;

        public bool IsResponsible(string idClass)
        {
            return Config.LocalData.Class_e_Mail.GUID == idClass;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = items
               };

               var elasticAgent = new ServiceAgentElastic(globals);

               messageOutput?.OutputInfo("Getting the emails-model...");

               var mailModelResult = await elasticAgent.GetEmailModel(items);

               result.ResultState = mailModelResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               messageOutput?.OutputInfo("Having the emails-model.");

               var template = "@NAME@ (Sended: @SENDED:dd.MM.yyyy HH:mm:ss@) | From: @FROM@ | To: @TO@";
               if (mailModelResult.Result.Template != null)
               {
                   template = mailModelResult.Result.Template.Val_String;
               }
               var regexName = new Regex(@"@(?i)(Name)@");
               var regexSended = new Regex(@"@(?i)(Sended)(:[yMd\.]+\s[hHms:-]+)?@");
               var regexSender = new Regex(@"@(?i)(FROM)@");
               var regexReceiver = new Regex(@"@(?i)(TO)@");

               foreach (var item in items)
               {
                   var name = template;
                   var sended = mailModelResult.Result.Sended.FirstOrDefault(att => att.ID_Object == item.GUID);
                   var senders = mailModelResult.Result.Senders.Where(sendr => sendr.ID_Object == item.GUID).ToList();
                   var receivers = mailModelResult.Result.Receivers.Where(recr => recr.ID_Object == item.GUID).ToList();


                   var match = regexName.Match(name);

                   if (match.Success)
                   {
                       name = name.Replace(match.Value, item.Name);
                   }
                   if (sended != null)
                   {
                       match = regexSended.Match(name);
                       if (match.Success)
                       {
                           if (match.Value.Contains(':'))
                           {
                               var splitted = match.Value.Split(':');
                               var format = splitted[1];
                               name = name.Replace(match.Value, sended.Val_Datetime.ToString());
                           }
                           else
                           {
                               name = name.Replace(match.Value, sended.Val_Datetime.ToString());
                           }
                       }
                   }

                   var sender = "";

                   if (senders.Any())
                   {
                       sender = string.Join(";", senders.Select(sendr => sendr.Name_Other));
                       match = regexSender.Match(name);
                       if (match.Success)
                       {
                           name = name.Replace(match.Value, sender);
                       }
                       
                   }

                   var receiver = "";
                   if (receivers.Any())
                   {
                       receiver = string.Join(";", receivers.Select(sendr => sendr.Name_Other));
                       match = regexReceiver.Match(name);
                       if (match.Success)
                       {
                           name = name.Replace(match.Value, receiver);
                       }
                   }

                   if (encodeName)
                   {
                       item.Val_String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(name)); ;
                   }
                   else
                   {
                       item.Name = name;
                   }


               }

               return result;
           });

            return taskResult;
        }

        public MailNameProvider(Globals globals)
        {
            this.globals = globals;
        }
    }
}
