﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class EmailTransformModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectAtt Template { get; set; }
        public List<clsOntologyItem> EmailItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> Sended { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> Senders { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> Receivers { get; set; } = new List<clsObjectRel>();

    }
}
