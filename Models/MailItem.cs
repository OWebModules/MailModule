﻿using MimeKit;
using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MailModule.Models
{
    [KendoGridConfig(width = "100%", height = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell, scrollable = true)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class MailItem
    {
        private MailsToElastic.Models.MailItem proxyItem { get; set; }
        private clsObjectRel mailItemToMail { get; set; }
        public clsOntologyItem mailOItem { get; set; }

        [KendoColumn(hidden = false, title = "Importance", Order = 1, filterable = true)]
        public string Folder
        {
            get
            {
                return proxyItem != null ? proxyItem.Folder : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Importance", Order = 2, filterable = true)]
        public string Importance
        {
            get
            {
                return proxyItem != null ? Enum.GetName(typeof(MessageImportance), proxyItem.Importance) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Priority", Order = 3, filterable = true)]
        public string Priority
        {
            get
            {
                return proxyItem != null ? Enum.GetName(typeof(MessagePriority), proxyItem.Priority) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Sender", Order = 4, filterable = true)]
        public string Sender
        {
            get
            {
                return proxyItem != null && proxyItem.Sender != null ? proxyItem.Sender.Address : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "From", Order = 5, filterable = true)]
        public string From
        {
            get
            {
                return proxyItem != null && proxyItem.From != null ? string.Join(", ", proxyItem.From.Select(from => from.Address)) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "To", Order = 6, filterable = true)]
        public string To
        {
            get
            {
                return proxyItem != null && proxyItem.To != null ? string.Join(", ", proxyItem.To.Select(to => to.Address)) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Cc", Order = 7, filterable = true)]
        public string Cc
        {
            get
            {
                return proxyItem != null && proxyItem.Cc != null ? string.Join(", ", proxyItem.Cc.Select(cc => cc.Address)) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Bcc", Order = 8, filterable = true)]
        public string Bcc
        {
            get
            {
                return proxyItem != null && proxyItem.Bcc != null ? string.Join(", ", proxyItem.Bcc.Select(bcc => bcc.Address)) : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Subject", Order = 9, filterable = true)]
        public string Subject
        {
            get
            {
                return proxyItem != null ? proxyItem.Subject : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Date", Order = 10, filterable = true, template = "#= Date != null ? kendo.toString(kendo.parseDate(Date), 'dd.MM.yyyy HH:mm:ss') : '' #", type = ColType.DateType)]
        public DateTimeOffset Date
        {
            get
            {
                return proxyItem != null ? proxyItem.Date : new DateTimeOffset();
            }
        }

        [KendoColumn(hidden = true)]
        public string MessageId
        {
            get
            {
                return proxyItem != null ? proxyItem.MessageId : string.Empty;
            }
        }

        [KendoColumn(hidden = false, title = "Attachement-Counts", Order = 11, filterable = true)]
        public int AttachmentCount
        {
            get
            {
                return proxyItem != null ? proxyItem.Attachments.Count : 0;
            }
        }

        [KendoColumn(hidden = false, title = "Body", Order = 11, filterable = true)]
        public string BodyShow
        {
            get
            {
                return "...";
            }
        }

        [KendoColumn(hidden = false, title = "Ontology-Item", Order = 0, filterable = true, template = "<i class='#= IsRelated == 'true' ? 'fa fa-check-square-o' : 'fa fa-square-o' # aria-hidden='true'></i>")]
        public bool IsRelated { get; private set; }
        
        public bool relate;
        [KendoColumn(hidden = false, Order = 12, filterable = false, title = "Relate", template = "<button type='button' #= IsRelated == 'true'  ? 'disabled' : '' # onclick='viewModel.relateMailItem(this);'>Relate</button>")]
        public bool Relate
        {
            get { return relate; }
            set
            {
                if (relate == value) return;

                relate = value;

            }
        }

        public bool select;
        [KendoColumn(hidden = false, Order = 13, filterable = false, title = "Select", template = "<button type='button' #= IsRelated == 'true'  ? '' : 'disabled' # onclick='viewModel.selectMailitem(this);'>Select</button>")]
        public bool Select
        {
            get { return select; }
            set
            {
                if (select == value) return;

                select = value;

            }
        }

        public bool apply;
        [KendoColumn(hidden = false, Order = 13, filterable = false, title = "Apply", template = "<button type='button' #= IsRelated == 'true'  ? '' : 'disabled' # onclick='viewModel.applyMailitem(this);'>Apply</button>")]
        public bool Apply
        {
            get { return apply; }
            set
            {
                if (apply == value) return;

                apply = value;

            }
        }

        [KendoColumn(hidden = true)]
        public string IdEmailItem
        {
            get
            {
                return mailItemToMail?.ID_Other;
            }
        }

        [KendoColumn(hidden = true)]
        public string NameEmailItem
        {
            get
            {
                return mailItemToMail?.Name_Other;
            }
        }

        [KendoColumn(hidden = true)]
        public string IdClassEmailItem
        {
            get
            {
                return mailItemToMail?.ID_Parent_Other;
            }
        }

        public string GetBody()
        {
            if (proxyItem == null) return string.Empty;
            return proxyItem.HtmlBody;
        }


        public MailsToElastic.Models.MailItem GetProxyItem()
        {
            return proxyItem;
        }

        public List<AttachmentViewItem> GetAttachmentList()
        {
            if (proxyItem == null || proxyItem.Attachments == null || !proxyItem.Attachments.Any()) return new List<AttachmentViewItem>();

            
            var result = proxyItem.Attachments.Select(att =>
            {
                var resultItem = new AttachmentViewItem
                {
                    Id = att.ContentId,
                    Type = att.ContentType.ToString()
                };
                var header = att.Headers.Where(head => head.Value.Contains("filename")).FirstOrDefault();

                if (header != null)
                {
                    var splitValue = header.Value.Split(';');
                    foreach (var valuePart in splitValue)
                    {
                        if (valuePart.Contains("filename="))
                        {
                            var fileName = valuePart.Replace("filename=","").Replace("\"","").Trim();
                            resultItem.FileName = fileName;
                            
                        }
                        else if (valuePart.Contains("size="))
                        {
                            var sizeS = valuePart.Replace("size=","").Replace("\"", "").Replace("\t","");

                            resultItem.Size = long.Parse(sizeS);
                        }
                    }
                    
                }

                return resultItem;
            }).ToList();
            return result;
        }

        public void SetMailItemToMail(clsObjectRel mailItemToMail, clsOntologyItem mailOItem)
        {
            this.mailItemToMail = mailItemToMail;
            this.mailOItem = mailOItem;

            IsRelated = this.mailItemToMail != null;
            
        }
        

        public MailItem(MailsToElastic.Models.MailItem mailItm, clsObjectRel mailItemToMail, clsOntologyItem mailOItm)
        {
            proxyItem = mailItm;
            this.mailItemToMail = mailItemToMail;
            this.mailOItem = mailOItm;

            IsRelated = this.mailItemToMail != null;
            
        }
    }
}
