﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class RequestGetOMailItems
    {
        public string IdMessage { get; set; }
        public string IdMailItem { get; set; }
    }
}
