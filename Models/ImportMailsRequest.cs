﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class ImportMailsRequest
    {
        public string IdConfiguration { get; set; }
        public string MasterPassword { get; set; }
        public bool DeleteMails { get; set; }

        public DateTime? StartScan { get; set; }
        public DateTime? EndScan { get; set; }

        public CancellationToken CancellationToken { get; set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
    }
}
