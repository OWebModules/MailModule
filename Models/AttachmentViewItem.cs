﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    [KendoGridConfig(width = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class AttachmentViewItem
    {
        [KendoColumn(hidden = true)]
        public string Id { get; set; }

        [KendoColumn(hidden = false, title = "Filename", Order = 1, filterable = true)]
        public string FileName { get; set; }

        [KendoColumn(hidden = false, title = "Type", Order = 2, filterable = true)]
        public string Type { get; set; }

        [KendoColumn(hidden = false, title = "Size", Order = 3, filterable = true)]
        public long Size { get; set; }

        private bool apply;
        [DataViewColumn(IsVisible = false, DisplayOrder = 0, CellType = CellType.Boolean)]
        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Apply", template = "<input type=\"checkbox\" #= Apply ? 'checked=\"checked\"' : \"\" # class=\"chkbxApply\" />", width = "80px", type = ColType.BooleanType)]
        public bool Apply
        {
            get { return apply; }
            set
            {
                apply = value;
            }
        }


    }
}
