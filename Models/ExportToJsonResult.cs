﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class ExportToJsonResult
    {
        public List<ExportedMailItemResult> ExportedMailItems { get; set; }
    }

    public class ExportedMailItemResult
    {
        public clsOntologyItem ExportResult { get; set; }
        public MailItem MailItem { get; set; }
    }
}
