﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class ResultGetOMailItems
    {
        public List<clsOntologyItem> OMailItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> MailItemsToMail { get; set; } = new List<clsObjectRel>();
    }
}
