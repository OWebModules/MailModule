﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public enum HostType
    {
        SmtpHost = 0,
        ImapHost = 1,
        Pop3Host = 2
    }
    public class MailHost
    {
        public string IdHost { get; set; }
        public string NameHost { get; set; }
        public string IdServerPort { get; set; }
        public string NameServerPort { get; set; }
        public string IdServer { get; set; }
        public string NameServer { get; set; }
        public string IdPort { get; set; }
        public int Port { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
        public string IdAttributeUseSSL { get; set; }
        public bool UseSSL { get; set; }
        public string IdAttributeValidateServerCertificate { get; set; }
        public bool ValidateServerCertificate { get; set; }
        public string IdSecureSocketOptions { get; set; }
        public string NameSecureSocketOptions { get; set; }
        public HostType HostType { get; set; }
        public string Password { get; set; }
    }
}
