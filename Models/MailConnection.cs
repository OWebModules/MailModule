﻿using ElasticSearchConfigModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class MailConnection
    {
        public ElasticSearchConfig ElasticsearchConfig { get; set; }
        public clsOntologyItem MailStorePath { get; set; }
        public List<MailHost> MailHosts { get; set; } = new List<MailHost>();
    }
}
