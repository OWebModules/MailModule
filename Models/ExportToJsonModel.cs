﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class ExportToJsonModel
    {
        public clsOntologyItem Config { get; set; }
        public clsObjectRel ConfigToMailModule { get; set; }
        public clsObjectRel ConfigToPath { get; set; }
    }
}
