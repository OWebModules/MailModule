﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MailModule.Models
{
    public class ExportToJsonRequest
    {
        public string IdConfig { get; private set; }

        public CancellationToken CancellationToken { get; private set; }
        public IMessageOutput MessageOutput {get; set;}

        public ExportToJsonRequest(string idConfig, CancellationToken cancellationToken)
        {
            IdConfig = idConfig;
            CancellationToken = cancellationToken;
        }
    }
}
