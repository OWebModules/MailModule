﻿using MailModule.Models;
using MailModule.Services;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Factories
{
    public class MailConnectionFactory
    {
        public async Task<ResultItem<MailConnection>> CreateConnection(MailConnectionConfig configRaw, Globals globals)
        {
            var elasticSearchConfig = new ElasticSearchConfigModule.ElasticSearchConfigController(globals);
            var taskResult = await Task.Run<ResultItem<MailConnection>>(async () =>
            {
                var result = new ResultItem<MailConnection>
                {
                    ResultState = globals.LState_Success.Clone()
                };


                var mailConfigPre = (from mailConfig in configRaw.MailConfigurations
                                     join mailStore in configRaw.MailConfigRels.Where(ix => ix.ID_RelationType == Config.LocalData.RelationType_Mailstore.GUID && ix.ID_Parent_Other == Config.LocalData.Class_Path.GUID) on mailConfig.GUID equals mailStore.ID_Object
                                 join smtpHost in configRaw.MailConfigRels.Where(ix => ix.ID_RelationType == Config.LocalData.RelationType_Smtp_Host.GUID) on mailConfig.GUID equals smtpHost.ID_Object into smtpHosts
                                 from smtpHost in smtpHosts.DefaultIfEmpty()
                                 join imapHost in configRaw.MailConfigRels.Where(ix => ix.ID_RelationType == Config.LocalData.RelationType_Imap_Host.GUID) on mailConfig.GUID equals imapHost.ID_Object into imapHosts
                                 from imapHost in imapHosts.DefaultIfEmpty()
                                 join pop3Host in configRaw.MailConfigRels.Where(ix => ix.ID_RelationType == Config.LocalData.RelationType_Pop3_Host.GUID) on mailConfig.GUID equals pop3Host.ID_Object into pop3Hosts
                                 from pop3Host in pop3Hosts.DefaultIfEmpty()
                                 select new
                                 {
                                    MailConfig = mailConfig,
                                    SmtpHost = smtpHost,
                                    ImapHost = imapHost,
                                    Pop3Host = pop3Host,
                                    MailStore = mailStore
                                 }).ToList().FirstOrDefault();

                if (mailConfigPre == null)
                {
                    result.ResultState = globals.LState_Nothing.Clone();
                    return result;
                }
                var mailConfigResult = await elasticSearchConfig.GetConfig(mailConfigPre.MailConfig.GUID,
                    Config.LocalData.RelationType_uses,
                    Config.LocalData.RelationType_uses,
                    globals.Direction_LeftRight);

                if (mailConfigResult.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                }

                result.Result = new MailConnection
                {
                    ElasticsearchConfig = mailConfigResult.Result,
                    MailStorePath = new clsOntologyItem
                    {
                        GUID = mailConfigPre.MailStore.ID_Other,
                        Name = mailConfigPre.MailStore.Name_Other,
                        GUID_Parent = mailConfigPre.MailStore.ID_Parent_Other,
                        Type = globals.Type_Object
                    },
                    MailHosts = new List<MailHost>()
                    
                };

                if (mailConfigPre.SmtpHost != null)
                {
                    var useSSLAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.SmtpHost.ID_Other &&  attr.ID_AttributeType == Config.LocalData.AttributeType_UseSSL.GUID);
                    var validateServerCertificateAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.SmtpHost.ID_Other && attr.ID_AttributeType == Config.LocalData.AttributeType_ValidateServerCertificate.GUID);
                    var secureSocketOptions = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.SmtpHost.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_SecureSocketOptions.GUID);
                    var serverPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.SmtpHost.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server_Port.GUID);
                    clsObjectRel serverPortToServer = null;
                    clsObjectRel serverPortToPort = null;
                    int Port = 0;
                    if (serverPort != null)
                    {
                        serverPortToServer = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server.GUID);
                        serverPortToPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Port.GUID);

                        if (serverPortToPort != null)
                        {
                            int.TryParse(serverPortToPort.Name_Other, out Port);
                        }
                    }

                    var secureUser = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.SmtpHost.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_user.GUID);

                    var smtpHost = new MailHost
                    {
                        HostType = HostType.SmtpHost,
                        IdHost = mailConfigPre.SmtpHost.ID_Other,
                        NameHost = mailConfigPre.SmtpHost.Name_Other,
                        IdAttributeUseSSL = useSSLAttribute?.ID_Attribute,
                        UseSSL = useSSLAttribute != null ? useSSLAttribute.Val_Bit.Value : false,
                        IdAttributeValidateServerCertificate = validateServerCertificateAttribute?.ID_Attribute,
                        ValidateServerCertificate = validateServerCertificateAttribute != null ? validateServerCertificateAttribute.Val_Bit.Value : false,
                        IdSecureSocketOptions = secureSocketOptions?.ID_Other,
                        NameSecureSocketOptions = secureSocketOptions?.Name_Other,
                        IdServerPort = serverPort?.ID_Other,
                        NameServerPort = serverPort?.Name_Other,
                        IdServer = serverPortToServer?.ID_Other,
                        NameServer = serverPortToServer?.Name_Other,
                        IdPort = serverPortToPort?.ID_Other,
                        Port = Port,
                        IdUser = secureUser?.ID_Other,
                        NameUser = secureUser?.Name_Other
                    };

                    result.Result.MailHosts.Add(smtpHost);
                }


                if (mailConfigPre.ImapHost != null)
                {
                    var useSSLAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.ImapHost.ID_Other && attr.ID_AttributeType == Config.LocalData.AttributeType_UseSSL.GUID);
                    var validateServerCertificateAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.ImapHost.ID_Other && attr.ID_AttributeType == Config.LocalData.AttributeType_ValidateServerCertificate.GUID);
                    var secureSocketOptions = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.ImapHost.ID_Other && rel.ID_Parent_Other ==  Config.LocalData.Class_SecureSocketOptions.GUID);
                    var serverPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.ImapHost.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server_Port.GUID);
                    clsObjectRel serverPortToServer = null;
                    clsObjectRel serverPortToPort = null;
                    int Port = 0;
                    if (serverPort != null)
                    {
                        serverPortToServer = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server.GUID);
                        serverPortToPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Port.GUID);

                        if (serverPortToPort != null)
                        {
                            int.TryParse(serverPortToPort.Name_Other, out Port);
                        }
                    }

                    var secureUser = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.ImapHost.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_user.GUID);

                    var imapHost = new MailHost
                    {
                        HostType = HostType.ImapHost,
                        IdHost = mailConfigPre.SmtpHost.ID_Other,
                        NameHost = mailConfigPre.SmtpHost.Name_Other,
                        IdAttributeUseSSL = useSSLAttribute?.ID_Attribute,
                        UseSSL = useSSLAttribute != null ? useSSLAttribute.Val_Bit.Value : false,
                        IdAttributeValidateServerCertificate = validateServerCertificateAttribute?.ID_Attribute,
                        ValidateServerCertificate = validateServerCertificateAttribute != null ? validateServerCertificateAttribute.Val_Bit.Value : false,
                        IdSecureSocketOptions = secureSocketOptions?.ID_Other,
                        NameSecureSocketOptions = secureSocketOptions?.Name_Other,
                        IdServerPort = serverPort?.ID_Other,
                        NameServerPort = serverPort?.Name_Other,
                        IdServer = serverPortToServer?.ID_Other,
                        NameServer = serverPortToServer?.Name_Other,
                        IdPort = serverPortToPort?.ID_Other,
                        Port = Port,
                        IdUser = secureUser?.ID_Other,
                        NameUser = secureUser?.Name_Other
                    };

                    result.Result.MailHosts.Add(imapHost);
                }

                if (mailConfigPre.Pop3Host != null)
                {
                    var useSSLAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.Pop3Host.ID_Other && attr.ID_AttributeType == Config.LocalData.AttributeType_UseSSL.GUID);
                    var validateServerCertificateAttribute = configRaw.ConnectionAtt.FirstOrDefault(attr => attr.ID_Object == mailConfigPre.Pop3Host.ID_Other && attr.ID_AttributeType == Config.LocalData.AttributeType_ValidateServerCertificate.GUID);
                    var secureSocketOptions = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.Pop3Host.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_SecureSocketOptions.GUID);
                    var serverPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.Pop3Host.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server_Port.GUID);
                    clsObjectRel serverPortToServer = null;
                    clsObjectRel serverPortToPort = null;
                    int Port = 0;
                    if (serverPort != null)
                    {
                        serverPortToServer = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Server.GUID);
                        serverPortToPort = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == serverPort.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_Port.GUID);

                        if (serverPortToPort != null)
                        {
                            int.TryParse(serverPortToPort.Name_Other, out Port);
                        }
                    }

                    var secureUser = configRaw.ConnectionRels.FirstOrDefault(rel => rel.ID_Object == mailConfigPre.Pop3Host.ID_Other && rel.ID_Parent_Other == Config.LocalData.Class_user.GUID);

                    var imapHost = new MailHost
                    {
                        HostType = HostType.ImapHost,
                        IdHost = mailConfigPre.SmtpHost.ID_Other,
                        NameHost = mailConfigPre.SmtpHost.Name_Other,
                        IdAttributeUseSSL = useSSLAttribute?.ID_Attribute,
                        UseSSL = useSSLAttribute != null ? useSSLAttribute.Val_Bit.Value : false,
                        IdAttributeValidateServerCertificate = validateServerCertificateAttribute?.ID_Attribute,
                        ValidateServerCertificate = validateServerCertificateAttribute != null ? validateServerCertificateAttribute.Val_Bit.Value : false,
                        IdSecureSocketOptions = secureSocketOptions?.ID_Other,
                        NameSecureSocketOptions = secureSocketOptions?.Name_Other,
                        IdServerPort = serverPort?.ID_Other,
                        NameServerPort = serverPort?.Name_Other,
                        IdServer = serverPortToServer?.ID_Other,
                        NameServer = serverPortToServer?.Name_Other,
                        IdPort = serverPortToPort?.ID_Other,
                        Port = Port,
                        IdUser = secureUser?.ID_Other,
                        NameUser = secureUser?.Name_Other
                    };

                    result.Result.MailHosts.Add(imapHost);
                }

                return result;
            });

            return taskResult;
        }
    }
}
