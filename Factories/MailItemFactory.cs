﻿using MailModule.Models;
using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Factories
{
    public class MailItemFactory
    {
        public async Task<ResultItem<List<Models.MailItem>>> CreateMailItemList(List<MailsToElastic.Models.MailItem> mailItemsRaw, ResultGetOMailItems oMailItems, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<Models.MailItem>>>(() =>
            {
                var result = new ResultItem<List<Models.MailItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<Models.MailItem>()
                };

                var oMailItms = (from mailItm in oMailItems.OMailItems
                                 join mailItemToMail in oMailItems.MailItemsToMail on mailItm.GUID equals mailItemToMail.ID_Object
                                 select new { mailItm, mailItemToMail }).ToList();
                var mailItems = (from mailItem in mailItemsRaw
                                join mailRel in oMailItms on mailItem.MessageId equals mailRel.mailItm.Name into mailRels
                                from mailRel in mailRels.DefaultIfEmpty()
                                select new { mailItem, mailRel }).ToList();

                result.Result = mailItems.Select(mailItm => new Models.MailItem(mailItm.mailItem, mailItm.mailRel?.mailItemToMail, mailItm.mailRel?.mailItm ) ).ToList();

                return result;
            });

            return taskResult;
        }
    }
}
