﻿using MailModule.Models;
using MailModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {
        public async Task<clsOntologyItem> RelateMailItem(MailItem mailItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(globals);

                var oItemsToSave = new List<clsOntologyItem>();
                var oRelsToSave = new List<clsObjectRel>();
                var oAttToSave = new List<clsObjectAtt>();

                
                var proxyItem = mailItem.GetProxyItem();

                var searchEmails = proxyItem.From.Select(fromItem => new clsOntologyItem
                {
                    Name = fromItem.Address,
                    GUID_Parent = Config.LocalData.Class_eMail_Address.GUID
                }).ToList();

                searchEmails.AddRange(proxyItem.To.Select(toItem => new clsOntologyItem
                {
                    Name = toItem.Address,
                    GUID_Parent = Config.LocalData.Class_eMail_Address.GUID
                }));

                var dbReaderEmailAddresses = new OntologyModDBConnector(globals);

                result = dbReaderEmailAddresses.GetDataObjects(searchEmails);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the Email-Addresses!";
                    return result;
                }

                var eMailAddressesToSavePre = (from fromItem in proxyItem.From
                                            join dbAddress in dbReaderEmailAddresses.Objects1 on fromItem.Address.ToLower() equals dbAddress.Name.ToLower() into dbAddresses
                                            from dbAddress in dbAddresses.DefaultIfEmpty()
                                            where dbAddress == null
                                            select fromItem.Address).ToList();

                eMailAddressesToSavePre.AddRange(from toItem in proxyItem.To
                                              join dbAddress in dbReaderEmailAddresses.Objects1 on toItem.Address.ToLower() equals dbAddress.Name.ToLower() into dbAddresses
                                              from dbAddress in dbAddresses.DefaultIfEmpty()
                                              where dbAddress == null
                                              select toItem.Address);

                var eMailAddressesToSave = eMailAddressesToSavePre.GroupBy(addr => addr).Select(addrGrp => new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = addrGrp.Key,
                    GUID_Parent = Config.LocalData.Class_eMail_Address.GUID,
                    Type = globals.Type_Object
                }).ToList();

                var addresses = dbReaderEmailAddresses.Objects1;
                addresses.AddRange(eMailAddressesToSave);

                var emailAddresses = addresses.GroupBy(adr => new { adr.GUID, adr.Name, adr.GUID_Parent, adr.Type }).Select(adr => new clsOntologyItem
                {
                    GUID = adr.Key.GUID,
                    Name = adr.Key.Name,
                    GUID_Parent = adr.Key.GUID_Parent,
                    Type = adr.Key.Type
                }).ToList();

                oItemsToSave.AddRange(eMailAddressesToSave);

                var mailOItem = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = mailItem.MessageId,
                    GUID_Parent = Config.LocalData.Class_Mail_Item.GUID,
                    Type = globals.Type_Object
                };

                var emailItem = new clsOntologyItem
                {
                    GUID = globals.NewGUID,
                    Name = !string.IsNullOrEmpty(mailItem.Subject) ? mailItem.Subject.Length > 255 ? mailItem.Subject.Substring(0, 254) : mailItem.Subject : mailItem.MessageId,
                    GUID_Parent = Config.LocalData.Class_e_Mail.GUID,
                    Type = globals.Type_Object
                };

                oItemsToSave.Add(mailOItem);
                oItemsToSave.Add(emailItem);

                

                var relMailItemToEmail = relationConfig.Rel_ObjectRelation(mailOItem, emailItem, Config.LocalData.RelationType_is);

                

                oRelsToSave.Add(relMailItemToEmail);

                var relSended = relationConfig.Rel_ObjectAttribute(emailItem, Config.LocalData.AttributeType_sended, mailItem.Date);

                oAttToSave.Add(relSended);

                foreach (var fromItm in proxyItem.From)
                {
                    var emailAddress = emailAddresses.First(adr => adr.Name.ToLower() == fromItm.Address.ToLower());
                    oRelsToSave.Add(relationConfig.Rel_ObjectRelation(emailItem, emailAddress, Config.LocalData.RelationType_Von));

                }

                foreach (var toItm in proxyItem.To)
                {
                    var emailAddress = emailAddresses.First(adr => adr.Name.ToLower() == toItm.Address.ToLower());
                    oRelsToSave.Add(relationConfig.Rel_ObjectRelation(emailItem, emailAddress, Config.LocalData.RelationType_An));
                }

                var dbWriter = new OntologyModDBConnector(globals);

                if (oItemsToSave.Any())
                {
                    result = dbWriter.SaveObjects(oItemsToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving MailItem or Email!";
                        return result;
                    }
                }
                
                if (oRelsToSave.Any())
                {
                    result = dbWriter.SaveObjRel(oRelsToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving MailItem to Email!";
                        return result;
                    }
                }
                
                if (oAttToSave.Any())
                {
                    result = dbWriter.SaveObjAtt(oAttToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving Sendedattribute!";
                        return result;
                    }
                }
                

                mailItem.SetMailItemToMail(relMailItemToEmail, mailOItem);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ResultGetOMailItems>> GetOMailItems(RequestGetOMailItems request)
        {
            var taskResult = await Task.Run<ResultItem<ResultGetOMailItems>>(() =>
            {
                var result = new ResultItem<ResultGetOMailItems>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ResultGetOMailItems()
                };

                var searchMailItems = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Mail_Item.GUID,
                        Name = request.IdMessage
                    }
                };

                var dbReaderOMailItems = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderOMailItems.GetDataObjects(searchMailItems);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Mail-Items Attributes";
                    return result;
                }

                result.Result.OMailItems = dbReaderOMailItems.Objects1;

                var searchOMailItemsToMail = result.Result.OMailItems.Select(itm => new clsObjectRel
                {
                    ID_Object = itm.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Mail_Item_is_e_Mail.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Mail_Item_is_e_Mail.ID_Class_Right
                }).ToList();

                var dbReaderOMailItemsToMails = new OntologyModDBConnector(globals);

                if (searchOMailItemsToMail.Any())
                {
                    result.ResultState = dbReaderOMailItemsToMails.GetDataObjectRel(searchOMailItemsToMail);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relation between Mailitems and Mails";
                        return result;
                    }

                    result.Result.MailItemsToMail = dbReaderOMailItemsToMails.ObjectRels;
                }
                

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<EmailTransformModel>> GetEmailModel(List<clsOntologyItem> emailItems)
        {
            var taskResult = await Task.Run<ResultItem<EmailTransformModel>>(() =>
           {
               var result = new ResultItem<EmailTransformModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new EmailTransformModel()
               };

               result.Result.Config = MailModule.NameTransform.Config.LocalData.Object_Baseconfig;

               var searchTemplate = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = result.Result.Config.GUID,
                       ID_AttributeType = NameTransform.Config.LocalData.AttributeType_Name_Template.GUID
                   }
               };

               var dbReaderTemplate = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTemplate.GetDataObjectAtt(searchTemplate);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Template!";
                   return result;
               }

               result.Result.Template = dbReaderTemplate.ObjAtts.FirstOrDefault();

               var searchMailItems = emailItems.Select(oMail => new clsOntologyItem
               {
                   GUID = oMail.GUID,
                   GUID_Parent = Config.LocalData.Class_e_Mail.GUID
               }).ToList();

               var dbReaderEmails = new OntologyModDBConnector(globals);
               if (searchMailItems.Any())
               {
                   result.ResultState = dbReaderEmails.GetDataObjects(searchMailItems);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Emails!";
                       return result;
                   }

                   result.Result.EmailItems = dbReaderEmails.Objects1;
               }

               var searchSended = result.Result.EmailItems.Select(email => new clsObjectAtt
               {
                   ID_Object = email.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_sended.GUID
               }).ToList();

               var dbReaderSended = new OntologyModDBConnector(globals);
               if (searchSended.Any())
               {
                   result.ResultState = dbReaderSended.GetDataObjectAtt(searchSended);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Sended-Datestamps!";
                       return result;
                   }

                   result.Result.Sended = dbReaderSended.ObjAtts;
               }

               var searchSender = result.Result.EmailItems.Select(email => new clsObjectRel
               {
                   ID_Object = email.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_e_Mail_Von_eMail_Address.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_e_Mail_Von_eMail_Address.ID_Class_Right
               }).ToList();

               var dbReaderSender = new OntologyModDBConnector(globals);

               if (searchSender.Any())
               {
                   result.ResultState = dbReaderSender.GetDataObjectRel(searchSender);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the senders!";
                       return result;
                   }

                   result.Result.Senders = dbReaderSender.ObjectRels;
               }


               var searchreceiver = result.Result.EmailItems.Select(email => new clsObjectRel
               {
                   ID_Object = email.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_e_Mail_An_eMail_Address.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_e_Mail_An_eMail_Address.ID_Class_Right
               }).ToList();

               var dbReaderReceiver = new OntologyModDBConnector(globals);

               if (searchreceiver.Any())
               {
                   result.ResultState = dbReaderReceiver.GetDataObjectRel(searchreceiver);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the receivers!";
                       return result;
                   }

                   result.Result.Receivers = dbReaderReceiver.ObjectRels;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<MailConnectionConfig>> GetMailConnectionConfig(clsOntologyItem mailModuleConfig = null)
        {
            var taskResult = await Task.Run<ResultItem<MailConnectionConfig>>(() =>
            {
                var result = new ResultItem<MailConnectionConfig>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new MailConnectionConfig()
                };

                var searchMailConnections = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_MailModule.GUID,
                        GUID = mailModuleConfig?.GUID
                    }
                };

                var dbReaderMailConnections = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderMailConnections.GetDataObjects(searchMailConnections);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Mail-Connection";
                    return result;
                }

                result.Result.MailConfigurations = dbReaderMailConnections.Objects1;
                var searchMailRels = result.Result.MailConfigurations.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_MailModule_Smtp_Host_Mail_Connection.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MailModule_Smtp_Host_Mail_Connection.ID_Class_Right
                }).ToList();

                searchMailRels.AddRange(result.Result.MailConfigurations.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_MailModule_Pop3_Host_Mail_Connection.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MailModule_Pop3_Host_Mail_Connection.ID_Class_Right
                }));

                searchMailRels.AddRange(result.Result.MailConfigurations.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_MailModule_Imap_Host_Mail_Connection.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MailModule_Imap_Host_Mail_Connection.ID_Class_Right
                }));

                searchMailRels.AddRange(result.Result.MailConfigurations.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_MailModule_Mailstore_Path.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_MailModule_Mailstore_Path.ID_Class_Right
                }));

                var dbReaderIndexes = new OntologyModDBConnector(globals);

                if (searchMailRels.Any())
                {
                    result.ResultState = dbReaderIndexes.GetDataObjectRel(searchMailRels);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                }

                result.Result.MailConfigRels = dbReaderIndexes.ObjectRels;

                var searchConnectionRels = result.Result.MailConfigRels.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Mail_Connection_uses_SecureSocketOptions.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Mail_Connection_uses_SecureSocketOptions.ID_Class_Right
                }).ToList();

                searchConnectionRels.AddRange(result.Result.MailConfigRels.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Mail_Connection_uses_Server_Port.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Mail_Connection_uses_Server_Port.ID_Class_Right
                }));

                searchConnectionRels.AddRange(result.Result.MailConfigRels.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Mail_Connection_authorized_by_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Mail_Connection_authorized_by_user.ID_Class_Right
                }));

                

                var dbReaderConnectionRels = new OntologyModDBConnector(globals);

                if (searchConnectionRels.Any())
                {
                    result.ResultState = dbReaderConnectionRels.GetDataObjectRel(searchConnectionRels);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                result.Result.ConnectionRels = dbReaderConnectionRels.ObjectRels;

                var dbReaderServerPort = new OntologyModDBConnector(globals);

                var searchServerPortToPortAndServer = result.Result.ConnectionRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Server_Port.GUID).Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Server_Port_belonging_Source_Server.ID_Class_Right
                }).ToList();

                searchServerPortToPortAndServer.AddRange(result.Result.ConnectionRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Server_Port.GUID).Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Server_Port_belonging_Source_Port.ID_Class_Right
                }));

                if (searchServerPortToPortAndServer.Any())
                {
                    result.ResultState = dbReaderServerPort.GetDataObjectRel(searchServerPortToPortAndServer);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Server- and Port-Configuration";
                        return result;
                    }
                }

                result.Result.ConnectionRels.AddRange(dbReaderServerPort.ObjectRels);

                var searchConnectionAttr = result.Result.MailConfigRels.Select(obj => new clsObjectAtt
                {
                    ID_Object = obj.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Mail_Connection_UseSSL.ID_AttributeType
                }).ToList();

                searchConnectionAttr.AddRange(result.Result.MailConfigRels.Select(obj => new clsObjectAtt
                {
                    ID_Object = obj.ID_Other,
                    ID_AttributeType = Config.LocalData.ClassAtt_Mail_Connection_ValidateServerCertificate.ID_AttributeType
                }));

                var dbReaderConnectionAtts = new OntologyModDBConnector(globals);

                if (searchConnectionAttr.Any())
                {
                    result.ResultState = dbReaderConnectionAtts.GetDataObjectAtt(searchConnectionAttr);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes of Configuration";
                        return result;
                    }
                    result.Result.ConnectionAtt = dbReaderConnectionAtts.ObjAtts;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportToJsonModel>> GetExportToJsonModel(ExportToJsonRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ExportToJsonModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ExportToJsonModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportTojsonModel(result.Result, dbReaderConfig, globals, nameof(ExportToJsonModel.Config));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchMailModule = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_belongs_to_MailModule.ID_RelationType,
                        ID_Parent_Other = ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_belongs_to_MailModule.ID_Class_Right
                    }
                };

                var dbReaderMailModule = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderMailModule.GetDataObjectRel(searchMailModule);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relation between config and MailModule!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportTojsonModel(result.Result, dbReaderMailModule, globals, nameof(ExportToJsonModel.ConfigToMailModule));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPath = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_export_to_Path.ID_RelationType,
                        ID_Parent_Other = ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_export_to_Path.ID_Class_Right
                    }
                };

                var dbReaderPath = new OntologyModDBConnector(globals);
                result.ResultState = dbReaderPath.GetDataObjectRel(searchPath);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the relation between config and path!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetExportTojsonModel(result.Result, dbReaderPath, globals, nameof(ExportToJsonModel.ConfigToPath));
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }

    public class MailConnectionConfig
    {
        public List<clsOntologyItem> MailConfigurations { get; set; }
        public List<clsObjectRel> MailConfigRels { get; set; }
        public List<clsObjectAtt> ConnectionAtt { get; set; }
        public List<clsObjectRel> ConnectionRels { get; set; }
     }
}
