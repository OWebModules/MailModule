﻿using ElasticSearchConfigModule;
using MailLib.Models;
using MailModule.Models;
using MailModule.Services;
using MailModule.Validation;
using MailsToElastic;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MailModule
{
    public class MailConnector : AppController
    {

        public async Task<ResultOItem<ClassObject>> GetOItem(string idObject)
        {
            var serviceElastic = new ServiceAgentElastic(Globals);
            var result = await serviceElastic.GetClassObject(idObject);
            return result;
        }

        public async Task<ResultItem<MailConnection>> GetConnection(clsOntologyItem configuration)
        {
            var serviceElastic = new ServiceAgentElastic(Globals);

            var result = new ResultItem<MailConnection>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new MailConnection()
            };

            if (configuration == null || configuration.GUID_Parent != Config.LocalData.Class_MailModule.GUID)
            {
                result.ResultState = Globals.LState_Relation.Clone();
            }

            var serviceResult = await serviceElastic.GetMailConnectionConfig(configuration);

            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;
                return result;
            }

            var factory = new Factories.MailConnectionFactory();
            var resultFactory = await factory.CreateConnection(serviceResult.Result, Globals);

            if (resultFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultFactory.ResultState;
                return result;
            }

            result.Result = resultFactory.Result;

            return result;
        }



        public async Task<ResultItem<ImportMailsResult>> ImportImapMails(ImportMailsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ImportMailsResult>>(async () =>
            {
                var result = new ResultItem<ImportMailsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ImportMailsResult()
                };


                if (string.IsNullOrEmpty(request.IdConfiguration))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Configuration-Id provided";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (!Globals.is_GUID(request.IdConfiguration))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid Configuration-Id provided";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var serviceAgent = new ServiceAgentElastic(Globals);

                var getConfigurationResult = await serviceAgent.GetClassObject(request.IdConfiguration);

                result.ResultState = getConfigurationResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting Configuration";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (getConfigurationResult.Result.ObjectItem.GUID_Parent == Config.LocalData.Class_Mail_Connection.GUID)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The given Configuration-Id is no Configuration-Id";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get Connection-Informations.");
                var getConnectionResult = await GetConnection(getConfigurationResult.Result.ObjectItem);

                result.ResultState = getConnectionResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Connection";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (!getConnectionResult.Result.MailHosts.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Hosts in configuration";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }


                var imapMailHost = getConnectionResult.Result.MailHosts.FirstOrDefault(host => host.HostType == HostType.ImapHost);

                if (imapMailHost == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Imap-Host in configuration";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Got Imapi MailHost");

                request.MessageOutput?.OutputInfo("Get User...");
                var getUserResult = await GetOItem(imapMailHost.IdUser);


                result.ResultState = getUserResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting User-Item";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have User");
                request.MessageOutput?.OutputInfo("Get Password...");

                var securityController = new SecurityModule.SecurityController(Globals);

                var passwordItemResult = await securityController.GetPassword(getUserResult.Result.ObjectItem, request.MasterPassword);

                result.ResultState = passwordItemResult.Result;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while gettint Password";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (!passwordItemResult.CredentialItems.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Password found";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Password");

                var mailToElastic = new MailsToElasticConnector();

                var loginRequest = new MailsToElastic.Models.ImportMailsRequest<ImapLoginRequest>
                {
                    AttachmentStorePath = getConnectionResult.Result.MailStorePath.Name,
                    ElasticIndex = getConnectionResult.Result.ElasticsearchConfig.NameIndex,
                    ElasticPort = getConnectionResult.Result.ElasticsearchConfig.Port,
                    ElasticServer = getConnectionResult.Result.ElasticsearchConfig.NameServer,
                    ElasticType = getConnectionResult.Result.ElasticsearchConfig.NameType,
                    MailLoginRequest = new ImapLoginRequest
                    {
                        Password = passwordItemResult.CredentialItems.First().Password.Name_Other,
                        Port = imapMailHost.Port,
                        Server = imapMailHost.NameServer,
                        Username = imapMailHost.NameUser,
                        UseSSL = imapMailHost.UseSSL,
                        ValidateServerCertificate = imapMailHost.ValidateServerCertificate
                    }
                };

                if (imapMailHost.IdSecureSocketOptions == Config.LocalData.Object_StartTls.GUID)
                {
                    loginRequest.MailLoginRequest.SecureSocketOptions = MailKit.Security.SecureSocketOptions.StartTls;
                }
                else if (imapMailHost.IdSecureSocketOptions == Config.LocalData.Object_SslOnConnect.GUID)
                {
                    loginRequest.MailLoginRequest.SecureSocketOptions = MailKit.Security.SecureSocketOptions.SslOnConnect;
                }

                request.MessageOutput?.OutputInfo("Import Mails...");
                var importMailsResult = await mailToElastic.ImportImapMails(loginRequest, new ImapGetMailsRequest
                {
                    DeleteMails = request.DeleteMails,
                    MessageOutput = request.MessageOutput,
                    Start = request.StartScan,
                    End = request.EndScan
                }, request.CancellationToken);


                result.ResultState = importMailsResult.Result;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while importing the mails!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Imported Mails");

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> RelateMailItem(MailItem mailItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                if (mailItem.IsRelated)
                {
                    return result;
                }

                var serviceAgent = new ServiceAgentElastic(Globals);

                var mailItemRaw = await serviceAgent.GetOMailItems(new RequestGetOMailItems
                {
                    IdMessage = mailItem.MessageId
                });

                result = mailItemRaw.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (mailItemRaw.Result.OMailItems.Any())
                {
                    mailItem.SetMailItemToMail(mailItemRaw.Result.MailItemsToMail.First(), mailItemRaw.Result.OMailItems.First());
                    return result;
                }

                var resultSaveItems = await serviceAgent.RelateMailItem(mailItem);

                result = resultSaveItems;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<Models.MailItem>>> GetMailItems(MailConnection mailConnection, DateRange dateRange, string searchText)
        {

            var mailToElastic = new MailsToElasticConnector();
            var elasticAgent = new ServiceAgentElastic(Globals);

            var resultConnector = await mailToElastic.GetMails(new MailsToElastic.Models.GetMailsRequest
            {
                ElasticIndex = mailConnection.ElasticsearchConfig.NameIndex,
                ElasticType = mailConnection.ElasticsearchConfig.NameType,
                ElasticPort = mailConnection.ElasticsearchConfig.Port,
                ElasticServer = mailConnection.ElasticsearchConfig.NameServer,
                DateRange = new MailsToElastic.Models.DateRange
                {
                    Start = dateRange.Start,
                    End = dateRange.End
                },
                SearchText = searchText
            });


            var result = new ResultItem<List<Models.MailItem>>
            {
                ResultState = resultConnector.Result.Clone()
            };

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var mailOItems = await elasticAgent.GetOMailItems(new RequestGetOMailItems());

            result.ResultState = mailOItems.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new Factories.MailItemFactory();
            var factoryResult = await factory.CreateMailItemList(resultConnector.Mails, mailOItems.Result, Globals);

            if (factoryResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = factoryResult.ResultState;
                return result;
            }

            result.Result = factoryResult.Result;

            return result;
        }

        public async Task<ResultItem<ExportToJsonResult>> ExportMailItemsToJson(ExportToJsonRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ExportToJsonResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ExportToJsonResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateExportToJsonRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Request validated.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Get model...");

                var modelResult = await elasticAgent.GetExportToJsonModel(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var path = modelResult.Result.ConfigToPath.Name_Other;
                try
                {
                    if (!Directory.Exists(path))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"The Path {path} does not exist!";
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The Path {path} is not valid: {ex.Message}";
                    return result;
                }


                request.MessageOutput?.OutputInfo("Get connection-config...");

                var getConnectionResult = await GetConnection(new clsOntologyItem
                {
                    GUID = modelResult.Result.ConfigToMailModule.ID_Other,
                    Name = modelResult.Result.ConfigToMailModule.Name_Other,
                    GUID_Parent = modelResult.Result.ConfigToMailModule.ID_Parent_Other,
                    Type = modelResult.Result.ConfigToMailModule.Ontology
                });

                result.ResultState = getConnectionResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have connection-config.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var mailsToElasticConnector = new MailsToElasticConnector();

                var getMailsRequest = new MailsToElastic.Models.GetMailsRequest
                {
                    ElasticIndex = getConnectionResult.Result.ElasticsearchConfig.NameIndex,
                    ElasticPort = getConnectionResult.Result.ElasticsearchConfig.Port,
                    ElasticServer = getConnectionResult.Result.ElasticsearchConfig.NameServer,
                    ElasticType = getConnectionResult.Result.ElasticsearchConfig.NameType
                };

                var page = 1;
                string scrollId = null;
                var loadNextPage = true;
                var pageSize = 500;

                try
                {

                    while (loadNextPage)
                    {
                        var filePath = Path.Combine(path,  $"{modelResult.Result.ConfigToMailModule.ID_Other}_{page}.json");

                        request.MessageOutput?.OutputInfo($"Get mails of page {page}...");
                        using (var streamWriter = new StreamWriter(filePath))
                        {
                            streamWriter.Write("[");
                            var getMailsResult = await mailsToElasticConnector.GetMails(getMailsRequest, scrollId, page, pageSize);
                            result.ResultState = getMailsResult.Result;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                            scrollId = getMailsResult.ScrollId;
                            page++;

                            for (int i = 0; i < getMailsResult.Mails.Count; i++)
                            {
                                var mail = getMailsResult.Mails[i];
                                var mailJson = Newtonsoft.Json.JsonConvert.SerializeObject(mail);
                                streamWriter.Write(mailJson);

                                if (i < getMailsResult.Mails.Count-1)
                                {
                                    streamWriter.Write(",");
                                }
                            }
                            streamWriter.Write("]");

                            loadNextPage = ((page - 1) * pageSize < getMailsResult.Total);
                        }

                        request.MessageOutput?.OutputInfo($"Exported {pageSize * page} mails...");
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ExportToJsonResult>> ImportMailItemsToJson(ExportToJsonRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<ExportToJsonResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new ExportToJsonResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateExportToJsonRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Request validated.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Get model...");

                var modelResult = await elasticAgent.GetExportToJsonModel(request);

                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var path = modelResult.Result.ConfigToPath.Name_Other;
                try
                {
                    if (!Directory.Exists(path))
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = $"The Path {path} does not exist!";
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"The Path {path} is not valid: {ex.Message}";
                    return result;
                }


                request.MessageOutput?.OutputInfo("Get connection-config...");

                var getConnectionResult = await GetConnection(new clsOntologyItem
                {
                    GUID = modelResult.Result.ConfigToMailModule.ID_Other,
                    Name = modelResult.Result.ConfigToMailModule.Name_Other,
                    GUID_Parent = modelResult.Result.ConfigToMailModule.ID_Parent_Other,
                    Type = modelResult.Result.ConfigToMailModule.Ontology
                });

                result.ResultState = getConnectionResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have connection-config.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User abort!");
                    return result;
                }

                var mailsToElasticConnector = new MailsToElasticConnector();

                var getMailsRequest = new MailsToElastic.Models.GetMailsRequest
                {
                    ElasticIndex = getConnectionResult.Result.ElasticsearchConfig.NameIndex,
                    ElasticPort = getConnectionResult.Result.ElasticsearchConfig.Port,
                    ElasticServer = getConnectionResult.Result.ElasticsearchConfig.NameServer,
                    ElasticType = getConnectionResult.Result.ElasticsearchConfig.NameType
                };

                var page = 1;
                string scrollId = null;
                var loadNextPage = true;
                var pageSize = 500;

                try
                {

                    while (loadNextPage)
                    {
                        foreach (var item in Directory.GetFiles(path))
                        {

                        }
                        var filePath = Path.Combine(path, $"{modelResult.Result.ConfigToMailModule.ID_Other}_{page}.json");

                        request.MessageOutput?.OutputInfo($"Get mails of page {page}...");
                        using (var streamWriter = new StreamWriter(filePath))
                        {
                            streamWriter.Write("[");
                            var getMailsResult = await mailsToElasticConnector.GetMails(getMailsRequest, scrollId, page, pageSize);
                            result.ResultState = getMailsResult.Result;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                                return result;
                            }
                            scrollId = getMailsResult.ScrollId;
                            page++;

                            for (int i = 0; i < getMailsResult.Mails.Count; i++)
                            {
                                var mail = getMailsResult.Mails[i];
                                var serializationSettings = new JsonSerializerSettings();
                                serializationSettings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;
                                var mailJson = Newtonsoft.Json.JsonConvert.SerializeObject(mail, serializationSettings);
                                streamWriter.Write(mailJson);

                                if (i < getMailsResult.Mails.Count - 1)
                                {
                                    streamWriter.Write(",");
                                }
                            }
                            streamWriter.Write("]");

                            loadNextPage = ((page - 1) * pageSize < getMailsResult.Total);
                        }

                        request.MessageOutput?.OutputInfo($"Exported {pageSize * page} mails...");
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }


                return result;
            });

            return taskResult;
        }

        public MailConnector(Globals globals) : base(globals)
        {
        }
    }


}
