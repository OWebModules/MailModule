﻿using MailModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateExportToJsonRequest(ExportToJsonRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid id!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetExportTojsonModel(ExportToJsonModel model, OntologyModDBConnector dbReader, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(ExportToJsonModel.Config))
            {
                if (dbReader.Objects1.Count == 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Config found!";
                    return result;
                }

                if (dbReader.Objects1.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "More than one config found!";
                    return result;
                }

                var config = dbReader.Objects1.First();

                if (config.GUID_Parent != ExportToJson.Config.LocalData.Class_Export_Mails_to_JSON.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided object is not of class {model.Config.Name}!";
                    return result;
                }

                model.Config = config;
            }
            else if (propertyName == nameof(ExportToJsonModel.ConfigToMailModule))
            {
                if (dbReader.ObjectRels.Count == 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No relation between config and MailModule found!";
                    return result;
                }

                if (dbReader.ObjectRels.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "More than one relation between config and MailModule found!";
                    return result;
                }

                var configToMailModule = dbReader.ObjectRels.First();

                if (configToMailModule.ID_Parent_Other != ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_belongs_to_MailModule.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is no relation between between config and MailModule!";
                    return result;
                }

                model.ConfigToMailModule = configToMailModule;
            }
            else if (propertyName == nameof(ExportToJsonModel.ConfigToPath))
            {
                if (dbReader.ObjectRels.Count == 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No relation between config and Path found!";
                    return result;
                }

                if (dbReader.ObjectRels.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "More than one relation between config and Path found!";
                    return result;
                }

                var configToPath = dbReader.ObjectRels.First();

                if (configToPath.ID_Parent_Other != ExportToJson.Config.LocalData.ClassRel_Export_Mails_to_JSON_export_to_Path.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided relation is no relation between between config and Path!";
                    return result;
                }

                model.ConfigToPath = configToPath;
            }

            return result;
        }
    }
}
